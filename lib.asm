section .text

GLOBAL string_length
GLOBAL print_char
GLOBAL print_newline
GLOBAL print_string

GLOBAL print_uint
GLOBAL print_int
GLOBAL string_equals
GLOBAL parse_uint
GLOBAL parse_int
GLOBAL read_word
GLOBAL string_copy
GLOBAL exit

 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    xor rdi, rdi
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	
    xor rax, rax
    .loop:
      mov rax, 0
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  call string_length
  mov    rdx,rax   
  mov     rsi, rdi 
  mov rdi, 0  
  mov     rax, 1    
  mov rdi, 1
  syscall
  ret

; Принимает код символа и выводит его в stdout
print_char:
	mov r8, 1
.print: mov rsi, rsp
	sub rsi, r8
	mov byte[rsi], dil
	mov rax, 1
	mov rdi, rax
	mov rdx, rdi
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
.start:	
	mov r8, 10
	mov r9, rsp
	mov r10, 0x30
	mov rax, rdi  	
	dec rsp
.div_op:
	xor rdx, rdx
	div r8
	add rdx, r10
	dec rsp
	mov byte[rsp], dl
	cmp rax, 0
	jne .div_op
	mov rdi, rsp
	call print_string
	mov rsp, r9
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
.start: mov rdx, 0
	cmp rdi, rdx
	jns .unsigned 	
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
.unsigned:
	call print_uint ;printing the positive number
	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov al, [rdi]
	cmp al, [rsi]
	jnz .end
	inc rsi
	add rdi, 1
	test al, al
	jne string_equals
	xor rax, rax
	add rax, 1
	ret
.end:
	xor rax, rax
	ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
.start:
 	push 0
	jmp .next
.cont:
	pop rax
	ret
.next:
	mov rsi, rsp
	mov rdi, 0
	mov rax, 0
	mov rdx, 1
	syscall
	jmp .cont
	

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
	mov r9, rsi
	mov r10, rdi
.loop:
	call read_char
	cmp rax, 0x20
	je .loop
	cmp rax, 0x9
	je .loop
	cmp rax, 0xA
	je .loop
.read_word:
	cmp rax, 0x0
	je .end
	cmp rax, 0x9
	je .end
	cmp rax, 0xA
	je .end
	cmp rax, 0x20
	je .end
	sub r9, 1
	cmp r9, 0
	je .stop
	mov byte[r8], al
	add r8, 1
	call read_char
	jmp .read_word
.stop:
	xor rax, rax
	ret
.end:
	mov byte[r8], 0
	mov rdx, r8
	sub rdx, r10
	mov rax, r10
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    	xor rdx, rdx
	mov rcx, rax
	xor rax, rax
	mov rsi, rdi

	
.loop:
	mov rdi, 0
	mov dil, byte[rsi+rdx]
	cmp dil, '0'
	jb .finish
	cmp dil, '9'
	ja .finish
	sub dil, '0'
	imul rax, 10
	add rax, rdi
	inc rdx
	dec rcx
	jne .loop
.finish:
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    	xor r8, r8
	mov al, byte[rdi]
	cmp al, "-"
	jne parse_uint
	add rdi, 1
	call parse_uint
	cmp rdx, r8
	je .end
	not rax
	inc rax
	add rdx, 1
.end:
	ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
.loop:
	call string_length   ; calls string length to rax
	add rax, 1
	cmp rdx, 0
	je .end
	cmp rax, rdx
	jg .end
	mov al, [rdi]
	mov [rsi], al
	cmp byte[rdi], 0
	jne .cont
	mov rax, rsi
	ret
.cont:
	inc rsi
	inc rdi
	dec rdx
	jmp .loop
	
.end:
	xor rax, rax
	ret
	
