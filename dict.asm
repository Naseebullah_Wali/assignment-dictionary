global find_word
extern string_equals
extern string_length


section .text

find_word: 
    mov rax, 0
.loop:
    test rsi, rsi 
    jz .end 
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .yes
    mov rsi, [rsi]
    jmp .loop
.yes:
    
    mov rdi, rsi
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 1
    ret
.end:
    mov rdi, 0
    ret
