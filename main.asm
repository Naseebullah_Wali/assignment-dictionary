section .text
%include "words.inc"
%include "colon.inc"
%define size_of_buffer 255

extern read_word
extern find_word
extern print_newline
extern print_string
extern string_length
extern exit
global _start

section .data
buf: times size_of_buffer db 0x00
fail: db "does_not_exist", 10, 0
win: db "write the key --> ", 0
success: db "it is: ", 0

    
section .text
_start:
    mov rdi, win
    call print_string

    
    
    mov rdi, buf
    mov rsi, size_of_buffer
    call read_word
    mov rdi, buf
    mov rsi, rubezhka
    call find_word
    test rdi, rdi
    jnz .good

    mov rdi, fail
    call print_string
    mov rax, 1
    call exit 

.good:
    push rdi
    mov rdi, success
    call print_string
    pop rdi
    call print_string
    mov rdi, 0
    call exit


